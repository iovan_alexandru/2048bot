﻿using Bot2048.Bot.AI.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bot2048.Bot
{
    public class SnakeEvaluator0 : IBoardEvaluator
    {
        private List<Tuple<int, int>> snakeCellListOrder = new List<Tuple<int, int>>();

        public SnakeEvaluator0()
        {
            for (var line = 0; line < 4; line++)
            {
                for (var column = 0; column < 4; column++)
                {
                    if (line % 2 == 0)
                    {
                        this.snakeCellListOrder.Add(new Tuple<int, int>(line, column));
                    }
                    else
                    {
                        this.snakeCellListOrder.Add(new Tuple<int, int>(line, 3 - column));
                    }
                }
            }
        }

        public int EvaluateBoard(Board board)
        {
            double score = 1;
            double last = 10000000;
            int i;
            for (i = 0; i < 16; i++)
            {
                var square = snakeCellListOrder[i];
                double current = board[square.Item1, square.Item2];
                if (current <= last && current > 0)
                {
                    score += current;
                    if (i == 4 && current == last)
                    {
                        i++;
                        break;
                    }
                    last = current;
                }
                else if (i == 0 || current == 0)
                {
                    break;
                }
                else //we are blocking the snake. If there's another way to increase the tail, we should award points for that
                {
                    var tailSquare = snakeCellListOrder.ElementAt(i - 1);
                    for (int j = i; j < 16; j++)
                    {
                        var trySquare = snakeCellListOrder.ElementAt(j);
                        if (Math.Abs(trySquare.Item1 - tailSquare.Item1) + Math.Abs(trySquare.Item2 - tailSquare.Item2) == 1)
                        {
                            int adjValue = board[trySquare.Item1, trySquare.Item2];
                            if (adjValue > 0 && adjValue <= last)
                            {
                                score += adjValue / 2;
                            }
                        }
                    }
                    break;
                }
            }
            int badScore = 0;
            for (; i < 16; i++)
            {
                var square = snakeCellListOrder.ElementAt(i);
                badScore += board[square.Item1, square.Item2] * 8;
            }
            score -= Math.Min(badScore, score / 8);
            return (int)score;
        }
    }
}
