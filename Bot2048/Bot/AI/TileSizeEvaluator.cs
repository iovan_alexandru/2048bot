﻿using Bot2048.Bot.AI.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bot2048.Bot.AI
{
    public class TileSizeEvaluator : IBoardEvaluator
    {
        public int EvaluateBoard(Board board)
        {
            int r = 0;
            const int boardSize = 4;
            for (int i = 0; i < boardSize; i++)
            {
                for (int j = 0; j < boardSize; j++)
                {
                    if (board[i, j] != 0)
                    {
                        if (board[i, j] >= 32)
                        {
                            r += 10;
                        }
                        else
                        {
                            r++;
                        }
                    }
                }
            }

            return r;
        }
    }
}
