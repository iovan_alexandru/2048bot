﻿using Bot2048.Bot.AI.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bot2048.Bot.AI
{
    public class PointEvaluator : IBoardEvaluator
    {
        public int EvaluateBoard(Board board)
        {
            var emptyCells = 0;
            var points = 0;
            var possibleFuturePoints = 0;

            var direction = new List<Tuple<int, int>>() 
            { 
                new Tuple<int, int>(-1, 0),
                new Tuple<int,int>(0, 1),
                new Tuple<int, int>(1,0),
                new Tuple<int, int>(0, -1)
            };

            for (var i = 0; i < 4; i++)
            {
                for (var j = 0; j < 4; j++)
                {
                    if (board[i, j] == 0)
                    {
                        emptyCells++;
                    }
                    else
                    {
                        points += board[i, j];
                        for (var k = 0; k < direction.Count; k++)
                        {
                            if (IsPositionValid(i + direction[k].Item1, j + direction[k].Item2) &&
                                board[i,j] == board[i+direction[k].Item1, j+direction[k].Item2])
                            {
                                possibleFuturePoints += (int)(board[i, j] * 0.6);
                            }
                        }
                    }
                }
            }

            var mean = points / (16-emptyCells); // there are 16 cells
            points += possibleFuturePoints + mean * emptyCells;
            
            return points;
        }

        private bool IsPositionValid(int x, int y)
        {
            return x >= 0 && x < 4 && y >= 0 && y < 4;
        }
    }
}
