﻿using Bot2048.Bot.AI;
using Bot2048.Bot.AI.Common;
using Bot2048.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bot2048.Bot.AI
{
    public class FirstBestMoveAIPlayer : BotPlayer
    {
        private IBoardEvaluator boardEvaluator;
        
        public FirstBestMoveAIPlayer(BrowserWrapper browserWrapper, IBoardEvaluator boardEvaluator)
            : base(browserWrapper)
        {
            this.boardEvaluator = boardEvaluator;
        }

        protected override Direction GetDirection(Board board)
        {
            double bestScore = int.MinValue;
            Direction bestDirection = Direction.None;
            foreach (Direction tryDirection in Enum.GetValues(typeof(Direction)))
            {
                if (tryDirection == Direction.None) continue;
                Board newBoard = board.MakeMove(tryDirection);
                if (newBoard.Equals(board)) continue;

                double newScore = this.boardEvaluator.EvaluateBoard(newBoard);
                //newBoard.Print();
                //Console.WriteLine("Value = {0}, Direction = {1}", newScore, (int)tryDirection);
                if (newScore > bestScore)
                {
                    bestDirection = tryDirection;
                    bestScore = newScore;
                }
            }

            //Console.WriteLine("Direction = {0}", (int)bestDirection);

            return bestDirection;
        }
    }
}
