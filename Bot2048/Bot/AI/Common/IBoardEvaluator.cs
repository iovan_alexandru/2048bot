﻿using Bot2048.Bot.AI.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bot2048.Bot.AI.Common
{
    public interface IBoardEvaluator
    {
        int EvaluateBoard(Board board);
    }
}
