﻿using Bot2048.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bot2048.Bot.AI.Common
{
    public class Board
    {
        private static readonly Random rand = new Random();
        private const int TARGET_TILE = 2048;

        private int[,] board = new int[4, 4];

        public Board(int[,] inputBoard)
        {
            for (var i = 0; i < 4; i++)
            {
                for (var j = 0; j < 4; j++)
                {
                    this.board[i, j] = inputBoard[i, j];
                }
            }
        }

        public Board(Board boardToClone)
        {
            for (var i = 0; i < 4; i++)
            {
                for (var j = 0; j < 4; j++)
                {
                    this.board[i, j] = boardToClone[i, j];
                }
            }
        }

        public Board MakeMove(Direction direction)
        {
            switch (direction)
            {
                case Direction.Up:
                    return MoveUp();
                case Direction.Down:
                    return MoveDown();
                case Direction.Left:
                    return MoveLeft();
                case Direction.Right:
                    return MoveRight();
            }

            return new Board(this.board);
        }

        public Board MoveRight()
        {
            var newBoard = new int[4, 4];

            for (var i = 0; i < 4; i++)
            {
                var oldRow = ArrayHelper.GetRow(this.board, i);
                var newRow = ArrayHelper.CollapseToLast(oldRow);
                ArrayHelper.ReplaceRow(newBoard, newRow, i);
            }

            return new Board(newBoard);
        }

        public Board MoveLeft()
        {
            var newBoard = new int[4, 4];

            for (var i = 0; i < 4; i++)
            {
                var oldRow = ArrayHelper.GetRow(this.board, i);
                var newRow = ArrayHelper.CollapseToFirst(oldRow);
                ArrayHelper.ReplaceRow(newBoard, newRow, i);
            }

            return new Board(newBoard);
        }

        public Board MoveDown()
        {
            var newBoard = new int[4, 4];

            for (var i = 0; i < 4; i++)
            {
                var oldColumn = ArrayHelper.GetColumn(this.board, i);
                var newColumn = ArrayHelper.CollapseToLast(oldColumn);
                ArrayHelper.ReplaceColumn(newBoard, newColumn, i);
            }

            return new Board(newBoard);
        }

        public Board MoveUp()
        {
            var newBoard = new int[4, 4];

            for (var i = 0; i < 4; i++)
            {
                var oldColumn = ArrayHelper.GetColumn(this.board, i);
                var newColumn = ArrayHelper.CollapseToFirst(oldColumn);
                ArrayHelper.ReplaceColumn(newBoard, newColumn, i);
            }

            return new Board(newBoard);
        }

        public override bool Equals(Object other)
        {
            Board otherBoard = other as Board;
            if (other == null) return false;

            for (var i = 0; i < 4; i++)
            {
                for (var j = 0; j < 4; j++)
                {
                    if (this.board[i, j] != otherBoard.board[i, j]) return false;
                }
            }

            return true;
        }

        public List<Board> GenerateAllNewTileBoards()
        {
            var boards = new List<Board>();
            var emptyCells = new List<Tuple<int, int>>();

            for (var i = 0; i < 4; i++)
            {
                for (var j = 0; j < 4; j++)
                {
                    if (this.board[i, j] == 0)
                    {
                        emptyCells.Add(new Tuple<int, int>(i, j));
                    }
                }
            }

            foreach (var emptyCell in emptyCells)
            {
                var newBoard = new Board(this.board);
                newBoard.board[emptyCell.Item1, emptyCell.Item2] = 2;
                boards.Add(newBoard);

                //newBoard = new Board(this.board);
                //newBoard.board[emptyCell.Item1, emptyCell.Item2] = 4;
                //boards.Add(newBoard);
            }

            return boards;
        }

        public Board GenerateNewTile()
        {
            var emptyCells = new List<Tuple<int, int>>();
            for (var i = 0; i < 4; i++)
            {
                for (var j = 0; j < 4; j++)
                {
                    if (this.board[i, j] == 0)
                    {
                        emptyCells.Add(new Tuple<int, int>(i, j));
                    }
                }
            }

            if (emptyCells.Count == 0)
            {
                return new Board(this.board);
            }

            int index = Board.rand.Next() % emptyCells.Count;
            Tuple<int, int> cell = emptyCells[index];
            var newBoard = ArrayHelper.Copy(this.board);
            newBoard[cell.Item1, cell.Item2] = Board.rand.Next() % 10 == 0 ? 4 : 2;

            return new Board(newBoard);
        }

        private bool ShouldForce2Tile(int[,] board)
        {
            var thresholdValue = 8;

            if (board[0, 0] == 0) // cell empty
            {
                var nextValueOnRow = ArrayHelper.GetRow(this.board, 0).FirstOrDefault(p => p > 0);
                var nextValueOnColumn = ArrayHelper.GetColumn(this.board, 0).FirstOrDefault(p => p > 0);
                if (nextValueOnRow > thresholdValue || nextValueOnColumn > thresholdValue)
                {
                    return true;
                }
            }

            return false;
        }

        public int this[int line, int column]
        {
            get
            {
                return this.board[line, column];
            }
            set
            {
                this.board[line, column] = value;
            }
        }

        public void Print()
        {
            Console.WriteLine(new string('=', 10));
            for (int i = 0; i < 4; i++)
            {
                Console.WriteLine(string.Format("{0,3} {1,3} {2,3} {3,3}", 
                    this.board[i, 0],
                    this.board[i, 1],
                    this.board[i, 2],
                    this.board[i, 3]));
            }
        }

        public int MaxValue()
        {
            var max = int.MinValue;
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    max = Math.Max(max, this.board[i, j]);
                }
            }

            return max;
        }

        internal bool IsWin()
        {
            for (var i = 0; i < 4; i++)
            {
                for (var j = 0; j < 4; j++)
                {
                    if (this.board[i, j] >= TARGET_TILE)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        internal List<Tuple<int,int>> GetEmptyCells()
        {
            List<Tuple<int, int>> emptyCells = new List<Tuple<int, int>>();
            for (var i = 0; i < 4; i++)
            {
                for (var j = 0; j < 4; j++)
                {
                    if (this.board[i, j] == 0)
                    {
                        emptyCells.Add(new Tuple<int, int>(i, j));
                    }
                }
            }

            return emptyCells;
        }
    }
}
