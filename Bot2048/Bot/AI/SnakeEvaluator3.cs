﻿using Bot2048.Bot.AI.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bot2048.Bot.AI
{
    public class SnakeEvaluator3 : IBoardEvaluator
    {
        private List<Tuple<int, int>> snakeCellListOrder = new List<Tuple<int, int>>();

        public SnakeEvaluator3()
        {
            for (var line = 0; line < 4; line++)
            {
                for (var column = 0; column < 4; column++)
                {
                    if (line % 2 == 0)
                    {
                        this.snakeCellListOrder.Add(new Tuple<int, int>(line, column));
                    }
                    else
                    {
                        this.snakeCellListOrder.Add(new Tuple<int, int>(line, 3 - column));
                    }
                }
            }
        }

        public int EvaluateBoard(Board board)
        {
            int score = 0;
            int previousCellValue = int.MaxValue;
            int snakeLength = board[0, 0] != 0 ? 1 : 0;
            int currentCellIndex = 0;
            int equalCellsInRow = 0;

            for (currentCellIndex = 0; currentCellIndex < 4; currentCellIndex++)
            {
                var cellPosition = this.snakeCellListOrder[currentCellIndex];
                var cellValue = board[cellPosition.Item1, cellPosition.Item2];
                if (cellValue == 0 || cellValue > previousCellValue) break;
                if (cellValue <= previousCellValue)
                {
                    equalCellsInRow = cellValue < previousCellValue ? 0 : equalCellsInRow + 1;
                    score += equalCellsInRow > 1 ? cellValue / 2 : cellValue;
                }

                previousCellValue = cellValue;
            }

            // give extra points for empty cells


            return score;
        }
    }
}
