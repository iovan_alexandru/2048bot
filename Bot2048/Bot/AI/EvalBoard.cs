﻿using Bot2048.Bot.AI.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bot2048.Bot.AI
{
    public class EvalBoard : IBoardEvaluatorExtended
    {
        private readonly Tuple<int, int>[] directions = new Tuple<int, int>[] 
            { 
                new Tuple<int,int>(0, -1),
                new Tuple<int,int>(1, 0),
                new Tuple<int,int>(0, 1),
                new Tuple<int, int>(-1, 0)
            };

        public Tuple<int, int> GetVector(int direction)
        {
            return directions[direction];
        }

        public int GetSmoothness(Board board)
        {
            double smoothness = 0;
            for (var x = 0; x < 4; x++)
            {
                for (var y = 0; y < 4; y++)
                {
                    if (board[x, y] > 0)
                    {
                        var value = board[x, y];
                        for (var direction = 1; direction <= 2; direction++)
                        {
                            var vector = GetVector(direction);
                            var targetCell = FindFarthestPosition(board, new Tuple<int, int>(x, y), vector);

                            if (IsWithinBounds(targetCell) && board[targetCell.Item1, targetCell.Item2] > 0)
                            {
                                var targetValue = board[targetCell.Item1, targetCell.Item2];
                                smoothness -= Math.Abs(value - targetValue);
                            }
                        }
                    }
                }
            }

            return (int)smoothness;
        }

        private Tuple<int, int> FindFarthestPosition(Board board, Tuple<int, int> cell, Tuple<int, int> vector)
        {
            Tuple<int, int> previous = null;
            do
            {
                previous = cell;
                cell = new Tuple<int, int>(previous.Item1 + vector.Item1, previous.Item2 + vector.Item2);
            } while (IsWithinBounds(cell) && board[cell.Item1, cell.Item2] > 0);

            return previous;
        }

        private bool IsWithinBounds(Tuple<int, int> cell)
        {
            return cell != null && cell.Item1 >= 0 && cell.Item1 <= 3 && cell.Item2 >= 0 && cell.Item2 <= 3;
        }

        private int GetMonotonicity(Board board)
        {
            // scores for all four directions
            var totals = new double[] { 0, 0, 0, 0 };

            // up or down direction
            for (var x = 0; x < 4; x++)
            {
                var current = 0;
                var next = current + 1;
                while (next < 4)
                {
                    while (next < 4 && board[x, next] == 0) next++;
                    if (next == 4) next--;

                    var currentValue = board[x, current] > 0 ? board[x, current] : 0;
                    var nextValue = board[x, next] > 0 ? board[x, next] : 0;
                    if (currentValue > nextValue)
                    {
                        totals[0] += nextValue - currentValue;
                    }
                    else if (nextValue > currentValue)
                    {
                        totals[1] += currentValue - nextValue;
                    }

                    current = next;
                    next++;
                }
            }

            // left or right direction
            for (var y = 0; y < 4; y++)
            {
                var current = 0;
                var next = current + 1;
                while (next < 4)
                {
                    while (next < 4 && board[next, y] == 0) next++;

                    if (next == 4) next--;

                    var currentValue = board[current, y] > 0 ? board[current, y] : 0;
                    var nextValue = board[next, y] > 0 ? board[next, y] : 0;

                    if (currentValue > nextValue)
                    {
                        totals[2] += nextValue - currentValue;
                    }
                    else if (nextValue > currentValue)
                    {
                        totals[3] += currentValue - nextValue;
                    }

                    current = next;
                    next++;
                }
            }

            return (int)(Math.Max(totals[0], totals[1]) + Math.Max(totals[2], totals[3]));
        }

        private int GetEmptyCellsNumber(Board board)
        {
            int nr = 0;
            for (var i = 0; i < 4; i++)
            {
                for (var j = 0; j < 4; j++)
                {
                    if (board[i, j] == 0)
                    {
                        nr++;
                    }
                }
            }

            return nr;
        }

        public int EvaluateBoard(Board board)
        {
            float smoothWeight = 0.1f, monotonyWeight = 1, emptyWeight = 2.7f, maxWeight = 1.0f;

            int value = (int)(GetSmoothness(board) * smoothWeight
                + monotonyWeight * GetMonotonicity(board)
                + Math.Pow(2, GetEmptyCellsNumber(board)) * emptyWeight
                + board.MaxValue() * maxWeight);

            return value;
        }

        public int GetIslands(Board board)
        {
            int islands = 0;

            bool[,] marks = new bool[4, 4];

            for (var x = 0; x < 4; x++)
            {
                for (var y = 0; y < 4; y++)
                {
                    if (board[x, y] > 0 && !marks[x, y])
                    {
                        islands++;
                        MarkTiles(board, x, y, marks, board[x, y]);
                    }
                }
            }

            return islands;
        }

        private void MarkTiles(Board board, int x, int y, bool[,] marks, int value)
        {
            if (x >= 0 && x <= 3 && y >= 0 && y <= 3 &&
                board[x, y] > 0 &&
                board[x, y] == value &&
                !marks[x, y])
            {
                marks[x, y] = true;

                for (var i = 0; i < 4; i++)
                {
                    MarkTiles(board, x + directions[i].Item1, y + directions[i].Item2, marks, value);
                }
            }
        }
    }
}
