﻿using Bot2048.Bot.AI.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bot2048.Bot.AI
{
    public class SumEvaluator : IBoardEvaluator
    {
        public int EvaluateBoard(Board board)
        {
            var sum = 0;
            for (var i = 0; i < 4; i++)
            {
                for (var j = 0; j < 4; j++)
                {
                    sum += board[i, j];
                }
            }
            
            return sum;
        }
    }
}
