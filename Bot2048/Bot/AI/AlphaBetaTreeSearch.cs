﻿using Bot2048.Bot.AI.Common;
using Bot2048.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bot2048.Bot.AI
{
    public class AlphaBetaTreeSearch : BotPlayer
    {
        private int[] PlayDirections = new int[] { 0, 1, 2, 3 };

        private IBoardEvaluatorExtended boardEvaluator;

        public AlphaBetaTreeSearch(BrowserWrapper browserWrapper, IBoardEvaluatorExtended boardEvaluator)
            : base(browserWrapper)
        {
            this.boardEvaluator = boardEvaluator;
        }

        private class Tile
        {
            public Tuple<int, int> Position { get; set; }
            public int Value { get; set; }
        }

        private class SearchResponse
        {
            public int Move { get; set; }

            public int Score { get; set; }

            public int Positions { get; set; }

            public int Cutoffs { get; set; }
        }

        protected override Direction GetDirection(Board board)
        {
            //perform iterative deepening on alpha beta serach
            DateTime startTime = DateTime.Now;
            TimeSpan searchTime = TimeSpan.FromMilliseconds(10);
            int depth = 0;
            SearchResponse best = this.Search(true, board, 5, -10000, 10000, 0, 0);
            /*
            do
            {
                var newBest = this.Search(true, board, depth, -10000, 10000, 0, 0);
                if (newBest.Move == -1)
                {
                    break;
                }
                else
                {
                    best = newBest;
                }

                depth++;
            } while (DateTime.Now - startTime < searchTime);
            */
            if (best==null || best.Move == -1) return Direction.None;

            return (Direction)best.Move;
        }

        private SearchResponse Search(bool isPlayerTurn, Board board, int depth, int alpha, int beta, int positions, int cutoffs)
        {
            int bestScore = 0, bestMove = -1;
            SearchResponse result = null;

            if (isPlayerTurn) // maximizing player
            {
                bestScore = alpha;
                foreach (var direction in PlayDirections)
                {
                    var newBoard = board.MakeMove((Direction)direction);
                    if (board.Equals(newBoard)) continue;

                    positions++;
                    if (newBoard.IsWin())
                    {
                        return new SearchResponse()
                        {
                            Move = direction,
                            Score = 10000,
                            Positions = positions,
                            Cutoffs = cutoffs
                        };
                    }

                    if (depth == 0)
                    {
                        result = new SearchResponse()
                        {
                            Move = direction,
                            Score = this.boardEvaluator.EvaluateBoard(newBoard)
                        };
                    }
                    else
                    {
                        result = Search(false, newBoard, depth - 1, bestScore, beta, positions, cutoffs);
                        if (result.Score > 9900)
                        {
                            result.Score--; // slightly penalize higher dept from win
                        }

                        positions = result.Positions;
                        cutoffs = result.Cutoffs;
                    }

                    if (result.Score > bestScore)
                    {
                        bestScore = result.Score;
                        bestMove = direction;
                    }

                    if (bestScore > beta)
                    {
                        cutoffs++;
                        return new SearchResponse()
                        {
                            Move = bestMove,
                            Score = beta,
                            Positions = positions,
                            Cutoffs = cutoffs
                        };
                    }
                }
            }
            else //computer's turn
            {
                bestScore = beta;

                //try a 2 and 4 in each cell and measure how annoying it is
                var emptyCells = board.GetEmptyCells();
                var candidates = new List<Tile>();
                var scores = new Dictionary<int, List<int>>()
                {
                    {2, new List<int>()},
                    {4, new List<int>()}
                };

                foreach (var value in scores.Keys)
                {
                    for (int index = 0; index < emptyCells.Count; index++)
                    {
                        var cell = emptyCells[index];
                        var newBoard = new Board(board);
                        newBoard[cell.Item1, cell.Item2] = value;
                        scores[value].Add(-this.boardEvaluator.GetSmoothness(newBoard)
                                          + this.boardEvaluator.GetIslands(newBoard));
                    }
                }

                //pick most annoying moves
                var maxScore = Math.Max(scores[2].Max(), scores[4].Max());
                foreach (var value in scores.Keys)
                {
                    for (var i = 0; i < scores[value].Count; i++)
                    {
                        if (scores[value][i] == maxScore)
                        {
                            candidates.Add(new Tile()
                            {
                                Position = emptyCells[i],
                                Value = value
                            });
                        }
                    }
                }

                //search on each candidate
                for (var i = 0; i < candidates.Count; i++)
                {
                    var position = candidates[i].Position;
                    var value = candidates[i].Value;
                    var newBoard = new Board(board);
                    newBoard[position.Item1, position.Item2] = value;

                    positions++;
                    result = Search(true, newBoard, depth, alpha, bestScore, positions, cutoffs);
                    positions = result.Positions;
                    cutoffs = result.Cutoffs;

                    if (result.Score < bestScore)
                    {
                        bestScore = result.Score;
                    }
                    if (bestScore < alpha)
                    {
                        cutoffs++;
                        return new SearchResponse()
                        {
                            Move = -1,
                            Score = alpha,
                            Positions = positions,
                            Cutoffs = cutoffs
                        };
                    }
                }
            }

            return new SearchResponse()
            {
                Move = bestMove,
                Score = bestScore,
                Positions = positions,
                Cutoffs = cutoffs
            };
        }
    }
}
