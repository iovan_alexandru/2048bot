﻿using Bot2048.Bot.AI.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bot2048.Bot.AI
{
    public class SnakeEvaluator : IBoardEvaluator
    {
        private List<Tuple<int, int>> snakeCellListOrder = new List<Tuple<int, int>>();

        public SnakeEvaluator()
        {
            for (var line = 0; line < 4; line++)
            {
                for (var column = 0; column < 4; column++)
                {
                    if (line % 2 == 0)
                    {
                        this.snakeCellListOrder.Add(new Tuple<int, int>(line, column));
                    }
                    else
                    {
                        this.snakeCellListOrder.Add(new Tuple<int, int>(line, 3 - column));
                    }
                }
            }
        }

        public int EvaluateBoard(Board board)
        {
            int score = 1;
            int previousCellValue = int.MaxValue;
            int i = 0;
            for (; i < 16; i++)
            {
                var cellPosition = this.snakeCellListOrder[i];
                var cellValue = board[cellPosition.Item1, cellPosition.Item2];
                if (cellValue <= previousCellValue && cellValue > 0)
                {
                    score += cellValue;
                    if (i == 4 && cellValue == previousCellValue)
                    {
                        i++;
                        break;
                    }

                    previousCellValue = cellValue;
                }
                else if (i == 0 || cellValue == 0)
                {
                    break;
                }
                else // we are blocking the snake
                {
                    var tailCell = this.snakeCellListOrder[i - 1];
                    for (var j = 1; j < 16; j++)
                    {
                        var tryCell = this.snakeCellListOrder[j];
                        if (Math.Abs(tailCell.Item1 - tryCell.Item1) + Math.Abs(tailCell.Item2 - tryCell.Item2) == 1)
                        {
                            int adjValue = board[tryCell.Item1, tryCell.Item2];
                            if (adjValue > 0 && adjValue <= previousCellValue)
                            {
                                score += adjValue / 2;
                            }
                        }
                    }
                }
            }

            int badScore = 0;
            for (; i < 16; i++)
            {
                var cellPosition = this.snakeCellListOrder[i];
                badScore += board[cellPosition.Item1, cellPosition.Item2] * 8;
            }
            score -= Math.Min(badScore, score / 8);

            return score;
        }
    }
}
