﻿using Bot2048.Bot.AI.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bot2048.Bot.AI
{
    public class SmoothnessEvaluator : IBoardEvaluator
    {
        public int EvaluateBoard(Board board)
        {
            var sum = 0;
            for (var line = 0; line < 4; line++)
            {
                for (var i = 0; i < 3; i++)
                {
                    sum += Math.Abs(board[line, i] - board[line, i + 1]);
                }
            }

            for (var column = 0; column < 4; column++)
            {
                for (var i = 0; i < 3; i++)
                {
                    sum += Math.Abs(board[i, column] - board[i + 1, column]);
                }
            }

            return sum;
        }
    }
}
