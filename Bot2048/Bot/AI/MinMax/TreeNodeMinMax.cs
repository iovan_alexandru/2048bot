﻿using Bot2048.Bot.AI.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bot2048.Bot.AI.MinMax
{
    internal class TreeNodeMinMax
    {
        private int _level;
        private TreeNodeMinMax _parentNode;
        private int? _value;
        private Board _gameBoard;
        private IBoardEvaluator _boardEvaluator;

        public int DirectionIndex { get; set; }

        public static int TreeNodeMaxLevel { get; set; }

        public bool HasValue
        {
            get
            {
                return this._value.HasValue;
            }
        }

        public int Value
        {
            get { return this._value.Value; }
            private set { this._value = value; }
        }

        public TreeNodeMinMax Parent
        {
            get { return this._parentNode; }
        }

        public int Level
        {
            get { return this._level; }
        }

        public TreeNodeMinMax(TreeNodeMinMax parent, Board gameBoard, IBoardEvaluator boardEvaluator)
        {
            this._parentNode = parent;
            this._level = this._parentNode == null ? 0 : this._parentNode.Level + 1;
            this._gameBoard = gameBoard;
            this._boardEvaluator = boardEvaluator;
            this.DirectionIndex = -1;
        }

        public LevelType LevelType
        {
            get
            {
                return this._level % 2 == 0 ? LevelType.Max : LevelType.Min;
            }
        }

        //Apply Alpha-Beta pruning algorithm
        public void Evaluate()
        {
            if (this.HasValue == true) return;
            if (this.Level == TreeNodeMinMax.TreeNodeMaxLevel)
            {
                this._value = this._boardEvaluator.EvaluateBoard(this._gameBoard);
                //this._gameBoard.Print();
                //Console.WriteLine("Value = {0}", this._value);
                return;
            }

            int numberOfDirections = 4;
            // generate children
            var nodeValue = this.LevelType == LevelType.Min ? int.MaxValue : int.MinValue;
            for (var i = 0; i < numberOfDirections; i++)
            {
                var actionDirection = (Direction)i;
                var boardAfterMove = this._gameBoard.MakeMove(actionDirection);
                if (this._gameBoard.Equals(boardAfterMove)) continue;
                var finalBoards = boardAfterMove.GenerateAllNewTileBoards();

                foreach (var finalBoard in finalBoards)
                {
                    var childNode = new TreeNodeMinMax(this, finalBoard, this._boardEvaluator);
                    childNode.Evaluate();
                    var value = childNode.Value;
                    if (this.LevelType == LevelType.Min && nodeValue > value)
                    {
                        nodeValue = value;
                        this.DirectionIndex = i;
                    }
                    else if (this.LevelType == LevelType.Max && nodeValue < value)
                    {
                        nodeValue = value;
                        this.DirectionIndex = i;
                    }
                }
            }

            if (nodeValue == int.MinValue || nodeValue == int.MaxValue)
            {
                nodeValue = this._boardEvaluator.EvaluateBoard(this._gameBoard);
            }

            this._value = nodeValue;
        }
    }
}
