﻿using Bot2048.Bot.AI.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bot2048.Bot.AI.AlphaBeta
{
    internal class TreeNode
    {
        private int _alfa, _beta, _level;
        private TreeNode _parentNode;
        private int? _value;
        private Board _gameBoard;
        private IBoardEvaluator _boardEvaluator;

        public int DirectionIndex { get; set; }

        public static int TreeNodeMaxLevel { get; set; }

        public int Alfa
        {
            get { return this._alfa; }
            private set
            {
                this._alfa = value;
            }
        }

        public int Beta
        {
            get { return this._beta; }
            private set
            {
                this._beta = value;
            }
        }

        public bool HasValue
        {
            get
            {
                return this._value.HasValue;
            }
        }

        public int Value
        {
            get { return this._value.Value; }
            private set { this._value = value; }
        }

        public TreeNode Parent
        {
            get { return this._parentNode; }
        }

        public int Level
        {
            get { return this._level; }
        }

        public TreeNode(TreeNode parent, Board gameBoard, IBoardEvaluator boardEvaluator)
        {
            this._parentNode = parent;
            this._level = this._parentNode == null ? 0 : this._parentNode.Level + 1;
            this._alfa = this._parentNode == null ? int.MinValue : this._parentNode._alfa;
            this._beta = this._parentNode == null ? int.MaxValue : this._parentNode._beta;
            this._gameBoard = gameBoard;
            this._boardEvaluator = boardEvaluator;
            this.DirectionIndex = -1;
        }

        public LevelType LevelType
        {
            get
            {
                return this._level % 2 == 0 ? LevelType.Max : LevelType.Min;
            }
        }

        /// <summary>
        /// Checks if the tree node should compute other child nodes
        /// </summary>
        /// <returns></returns>
        private bool CanPrune()
        {
            return this._alfa == this._beta || this._alfa > this._beta;
        }

        //Apply Alpha-Beta pruning algorithm
        public void Evaluate()
        {
            if (this.HasValue == true) return;
            if (this.Level == TreeNode.TreeNodeMaxLevel)
            {
                this._value = this._boardEvaluator.EvaluateBoard(this._gameBoard);
                //this._gameBoard.Print();
                //Console.WriteLine("Value = {0}", this._value);
                return;
            }

            if (OptimizeTopLevel())
            {
                return;
            }

            int numberOfDirections = 4;
            // var skipped = 0;
            // generate children
            var nodeValue = this.LevelType == LevelType.Min ? int.MaxValue : int.MinValue;
            for (var i = 0; i < numberOfDirections && !this.CanPrune(); i++)
            {
                var actionDirection = (Direction)i;
                var boardAfterMove = this._gameBoard.MakeMove(actionDirection);
                if (this._gameBoard.Equals(boardAfterMove)) continue;

                var finalBoards = boardAfterMove.GenerateAllNewTileBoards();
                foreach (var finalBoard in finalBoards)
                {
                    var childNode = new TreeNode(this, finalBoard, this._boardEvaluator);
                    childNode.Evaluate();
                    var value = childNode.Value;
                    if (this.LevelType == LevelType.Min && nodeValue > value)
                    {
                        nodeValue = value;
                        this.DirectionIndex = i;
                    }
                    else if (this.LevelType == LevelType.Max && nodeValue < value)
                    {
                        nodeValue = value;
                        this.DirectionIndex = i;
                    }

                    UpdateBoundaries(value);
                }
            }

            if (nodeValue == int.MinValue || nodeValue == int.MaxValue)
            {
                nodeValue = this._boardEvaluator.EvaluateBoard(this._gameBoard);
            }

            this._value = nodeValue;
        }

        private bool OptimizeTopLevel()
        {
            if (this._level > 0)
            {
                return false;
            }

            var index = -1;
            var nr = 0;
            for (var i = 0; i < 4; i++)
            {
                var direction = (Direction)i;
                if (this._gameBoard.Equals(this._gameBoard.MakeMove(direction)))
                {
                    nr++;
                }
                else
                {
                    index = i;
                }
            }

            if (nr == 3)
            {
                this.DirectionIndex = index;
                return true;
            }

            return false;
        }

        private void UpdateBoundaries(int value)
        {
            if (this.LevelType == LevelType.Min && this._beta > value)
            {
                this._beta = value;
            }
            else if (this.LevelType == LevelType.Max && this._alfa < value)
            {
                this._alfa = value;
            }
        }
    }
}
