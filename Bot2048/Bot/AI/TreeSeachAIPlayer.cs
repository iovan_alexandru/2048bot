﻿using Bot2048.Bot.AI.AlphaBeta;
using Bot2048.Bot.AI.Common;
using Bot2048.Bot.AI.MinMax;
using Bot2048.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bot2048.Bot.AI
{
    public class TreeSeachAIPlayer : BotPlayer
    {
        private IBoardEvaluator boardEvaluator;

        public TreeSeachAIPlayer(BrowserWrapper browserWrapper, IBoardEvaluator boardEvaluator)
            : base(browserWrapper)
        {
            this.boardEvaluator = boardEvaluator;
        }

        protected override Direction GetDirection(Board board)
        {
            TreeNode.TreeNodeMaxLevel = 4;
            TreeNode treeNode = new TreeNode(null, board, boardEvaluator);
            treeNode.Evaluate();
            var index = treeNode.DirectionIndex;
            if (index < 0 || index >= 4)
            {
                return Direction.None;
            }

            return (Direction)index;
        }
    }
}
