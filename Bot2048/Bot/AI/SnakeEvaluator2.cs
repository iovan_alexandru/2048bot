﻿using Bot2048.Bot.AI.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bot2048.Bot.AI
{
    public class SnakeEvaluator2 : IBoardEvaluator
    {
        private List<Tuple<int, int>> snakeCellListOrder = new List<Tuple<int, int>>();

        public SnakeEvaluator2()
        {
            for (var line = 0; line < 4; line++)
            {
                for (var column = 0; column < 4; column++)
                {
                    if (line % 2 == 0)
                    {
                        this.snakeCellListOrder.Add(new Tuple<int, int>(line, column));
                    }
                    else
                    {
                        this.snakeCellListOrder.Add(new Tuple<int, int>(line, 3 - column));
                    }
                }
            }
        }

        private double Score(Board board)
        {
            double total = 0;
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    if (board[i, j] > 0) total += board[i, j] * Math.Log(board[i, j]) / Math.Log(2);
                }
            }
            return total;
        }

        public int EvaluateBoard(Board board)
        {
            double score = Score(board);
            double last = 10000000;
            for (int i = 0; i < 16; i++)
            {
                var square = snakeCellListOrder[i];
                double current = board[square.Item2, square.Item1];
                if (current > last)
                {
                    score -= current * Math.Log(current - last) / Math.Log(2);
                }
                if (current == 0 && last > 0 && i > 0)
                {
                    score -= last * Math.Log(last) / Math.Log(2) / 2;
                }
                last = current;
            }
            return (int)score;
        }
    }
}
