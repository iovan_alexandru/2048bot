﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bot2048.Bot
{
    public enum Direction
    {
        Up,
        Left,
        Right,
        Down,
        None,
        Skip
    }
}
