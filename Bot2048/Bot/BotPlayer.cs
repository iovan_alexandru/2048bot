﻿using Bot2048.Bot.AI;
using Bot2048.Bot.AI.Common;
using Bot2048.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bot2048.Bot
{
    public abstract class BotPlayer
    {
        private const int BOARD_HEIGHT = 4;
        private const int BOARD_WIDTH = 4;

        private BrowserWrapper browserWrapper { get; set; }
        private bool isTaskRunning, isBrowserInitialized;
        private CancellationTokenSource taskCancellationTokenSource;
        private int maxReachedTile;

        public event EventHandler<int> OnSuperiorTileAchieved;
        public event EventHandler<EventArgs> OnDocumentLoaded;
        public event EventHandler<EventArgs> OnGameFinished;

        public BotPlayer(BrowserWrapper browserWrapper)
        {
            this.browserWrapper = browserWrapper;
            this.isTaskRunning = false;
            this.isBrowserInitialized = false;
            this.isTaskRunning = false;
            this.browserWrapper.OnDocumentLoaded += BrowserWrapper_OnDocumentLoaded;
            this.maxReachedTile = 0;
        }

        private void BrowserWrapper_OnDocumentLoaded(object sender, HtmlElementEventArgs e)
        {
            this.isBrowserInitialized = true;
            this.StartNewGame();

            if (this.OnDocumentLoaded != null)
            {
                this.OnDocumentLoaded(sender, e);
            }

            if (this.isTaskRunning)
            {
                CreateSolverTask();
            }
        }

        public void Start()
        {
            this.isTaskRunning = true;
            this.maxReachedTile = 0;
            StartNewGame();

            if (isBrowserInitialized)
            {
                CreateSolverTask();
            }
        }

        public void Stop()
        {
            if (this.taskCancellationTokenSource != null)
            {
                this.taskCancellationTokenSource.Cancel();
            }
        }

        public void StartNewGame()
        {
            this.browserWrapper.StartNewGame();
        }

        private void SignalNewTileReach()
        {
            if (this.OnSuperiorTileAchieved != null)
            {
                this.OnSuperiorTileAchieved(this, this.maxReachedTile);
            }
        }

        private void CreateSolverTask()
        {
            this.taskCancellationTokenSource = new CancellationTokenSource();
            Task.Run(() =>
            {
                this.isTaskRunning = true;
                Run2048Solver();
            }, taskCancellationTokenSource.Token)
            .ContinueWith((t) =>
            {
                this.isTaskRunning = false;
            }, TaskContinuationOptions.OnlyOnCanceled);
        }

        private void SimulateGameEnded()
        {
            if (this.OnGameFinished != null)
            {
                this.OnGameFinished(this, null);
            }
        }

        private void Run2048Solver()
        {
            TimeSpan moveDelay = TimeSpan.FromMilliseconds(ConfigReader.MoveDelayMilliseconds);
            DateTime lastMove = DateTime.Now.AddMilliseconds(-moveDelay.TotalMilliseconds);
            Board oldBoard = null, currentBoard = null;

            while (!taskCancellationTokenSource.Token.IsCancellationRequested)
            {
                if ((DateTime.Now - lastMove) > moveDelay)
                {
                    int[,] board = this.browserWrapper.GetBoardConfiguration();
                    currentBoard = new Board(board);
                    if (currentBoard.Equals(oldBoard))
                    {
                        continue;
                    }
                    else
                    {
                        oldBoard = currentBoard;
                    }

                    CheckMaxTile(board);
                    Direction key = GetDirection(currentBoard);
                    if (key == Direction.None)
                    {
                        PrintBoard(board);
                        this.SimulateGameEnded();                        
                        this.Stop();
                        continue;
                    }

                    this.browserWrapper.SimulateKey(key);

                    lastMove = DateTime.Now;
                }
            }
        }

        private void PrintBoard(int[,] board)
        {
            var gameBoard = new Board(board);
            gameBoard.Print();
        }

        private void CheckMaxTile(int[,] board)
        {
            var currentMax = 0;
            for (var i = 0; i < BOARD_HEIGHT; i++)
            {
                for (var j = 0; j < BOARD_WIDTH; j++)
                {
                    if (board[i, j] > currentMax)
                    {
                        currentMax = board[i, j];
                    }
                }
            }

            if (currentMax > this.maxReachedTile)
            {
               this.maxReachedTile = currentMax;
                SignalNewTileReach();
            }
        }

        protected abstract Direction GetDirection(Board board);
    }
}
