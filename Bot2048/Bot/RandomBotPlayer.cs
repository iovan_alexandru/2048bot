﻿using Bot2048.Bot.AI.Common;
using Bot2048.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bot2048.Bot
{
    public class RandomBotPlayer : BotPlayer
    {
        public RandomBotPlayer(BrowserWrapper browserWrapper)
            : base(browserWrapper)
        {
        }

        protected override Direction GetDirection(Board board)
        {
            // Currently GetMoveKey generates random moves
            Random rand = new Random();
            switch (rand.Next() % 2)
            {
                case 0:
                    return Direction.Up;
                case 1:
                    return Direction.Left;
                case 2:
                    return Direction.Down;
                case 3:
                    return Direction.Right;
            }

            return Direction.Right;
        }
    }
}
