﻿using Bot2048.Bot;
using Bot2048.Bot.AI;
using Bot2048.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bot2048
{
    public partial class ApplicationWindow : Form
    {
        private System.Windows.Forms.Timer elapsedSecondsTimer;
        private DateTime gameStartTime;
        private BotPlayer botPlayer;
        private int gameTimeLimit;
        private DateTime lastPlayedSound;
        private WMPLib.WindowsMediaPlayer soundPlayer;

        public ApplicationWindow()
        {
            InitializeComponent();
            DoInitialSetup();
            soundPlayer = new WMPLib.WindowsMediaPlayer();
        }

        private void DoInitialSetup()
        {
            this.gameTimeLimit = ConfigReader.DefaultGameplayTime;
            this.GameplayTimeTB.Text = this.gameTimeLimit.ToString();

            this.elapsedSecondsTimer = new System.Windows.Forms.Timer()
            {
                Interval = 1000
            };
            this.elapsedSecondsTimer.Tick += ElapsedSecondsTimer_Tick;

            this.botPlayer = new FirstBestMoveAIPlayer(new BrowserWrapper(this.GameContainerWB), new EvalBoard());
            this.botPlayer.OnSuperiorTileAchieved += BotPlayer_OnSuperiorTileAchieved;
            this.botPlayer.OnDocumentLoaded += BotPlayer_OnDocumentLoaded;
            this.botPlayer.OnGameFinished += BotPlayer_OnGameFinished;

            DeactivateApplication();
        }

        private void BotPlayer_OnGameFinished(object sender, EventArgs e)
        {
            PlaySound(@"Content\Sounds\whawha.mp3");
        }

        private void PlaySound(string url)
        {
            try
            {
                soundPlayer.URL = url;
                soundPlayer.controls.play();
            }
            catch (Exception ex)
            {
                // doesn't matter if you can't play sounds
            }
        }

        private void DeactivateApplication()
        {
            this.StartBtn.Enabled = false;
            this.StopBtn.Enabled = false;
            this.ResetBrowserBtn.Enabled = false;
            this.ElapsedSecondsLbl.Enabled = false;
        }

        private void ActivateApplication()
        {
            this.StartBtn.Enabled = true;
            this.StopBtn.Enabled = true;
            this.ResetBrowserBtn.Enabled = true;
            this.ElapsedSecondsLbl.Enabled = true;
        }

        private void BotPlayer_OnDocumentLoaded(object sender, EventArgs e)
        {
            ActivateApplication();
        }

        private void BotPlayer_OnSuperiorTileAchieved(object sender, int score)
        {
            // make sound...
            Console.WriteLine(string.Format("{0} tile reached.", score));
            PlaySound(@"Content\Sounds\beep.mp3");
            ScoreLbl.Invoke((Action)(() =>
            {
                ScoreLbl.Text = score.ToString();
            }));
        }

        private void ElapsedSecondsTimer_Tick(object sender, EventArgs e)
        {
            double elapsedSeconds = Math.Round((DateTime.Now - gameStartTime).TotalSeconds);
            ElapsedSecondsLbl.Text = string.Format("{0:0}", elapsedSeconds);

            if (elapsedSeconds > this.gameTimeLimit)
            {
                StopGame();
            }
        }

        private void StopGame()
        {
            this.botPlayer.Stop();
            LeaveRunState();
        }

        private void StartBtn_Click(object sender, EventArgs e)
        {
            soundPlayer = new WMPLib.WindowsMediaPlayer();
            ScoreLbl.Text = "0";
            this.gameTimeLimit = GetGameTimeLimit();
            if (this.gameTimeLimit <= 0)
            {
                MessageBox.Show("Invalid time limit. The time limit must be a positive number.");
                return;
            }

            EnterRunState();
            this.botPlayer.Start();
        }

        private void EnterRunState()
        {
            this.GameplayTimeTB.Enabled = false;
            ResetTimer();
            this.StartBtn.Enabled = false;
            this.StopBtn.Enabled = true;
            this.ResetBrowserBtn.Enabled = false;
        }

        private void LeaveRunState()
        {
            this.GameplayTimeTB.Enabled = true;
            this.StartBtn.Enabled = true;
            this.StopBtn.Enabled = false;
            this.elapsedSecondsTimer.Stop();
            this.ResetBrowserBtn.Enabled = true;
        }

        private int GetGameTimeLimit()
        {
            var gameTimeLimitString = this.GameplayTimeTB.Text.Trim();
            int timeLimit;
            if (!int.TryParse(gameTimeLimitString, out timeLimit))
            {
                return -1;
            }

            return timeLimit;
        }

        private void ResetTimer()
        {
            this.ElapsedSecondsLbl.Text = "0";
            this.elapsedSecondsTimer.Start();
            this.gameStartTime = DateTime.Now;
        }

        private void StopBtn_Click(object sender, EventArgs e)
        {
            StopGame();
        }

        private void ResetBrowserBtn_Click(object sender, EventArgs e)
        {
            if (this.botPlayer != null)
            {
                this.botPlayer.StartNewGame();
            }
        }
    }
}
