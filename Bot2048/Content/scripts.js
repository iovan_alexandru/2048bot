﻿function resetGame() {
    document.getElementsByClassName("restart-button")[0].click();
}

function fireKey(element, keyCode) {
    var eventObj = document.createEvent("Events");
    eventObj.initEvent("keydown", true, true);
    eventObj.which = keyCode;
    element.dispatchEvent(eventObj);
}

function simulateLeft() {
    var LEFT_KEY_CODE = 37;
    fireKey(document, LEFT_KEY_CODE);
}

function simulateUp() {
    var UP_KEY_CODE = 38;
    fireKey(document, UP_KEY_CODE);
}

function simulateRight() {
    var RIGHT_KEY_CODE = 39;
    fireKey(document, RIGHT_KEY_CODE);
}

function simulateDown() {
    var DOWN_KEY_CODE = 40;
    fireKey(document, DOWN_KEY_CODE);
}

function startNewGame() {
    var restartLink = document.getElementsByClassName("restart-button")[0];
    
    var eventObj = document.createEvent("Events");
    eventObj.initEvent("click", true, true);
    restartLink.dispatchEvent(eventObj);
}

function getBoard() {
    var tileBoard = [[0, 0, 0, 0],
                     [0, 0, 0, 0],
                     [0, 0, 0, 0],
                     [0, 0, 0, 0]];
    var tiles = document.getElementsByClassName("tile-container")[0].childNodes;
    var tilePositionRegex = /^tile-position-[0-9]-[0-9]/;
    for (var i = 0; i < tiles.length; i++) {
        var div = tiles[i];
        var classesString = div.getAttribute("class");
        var classes = classesString.split(' ');
        var x = -1, y = -1, value = -1;

        for (var j = 0; j < classes.length; j++) {
            if (tilePositionRegex.test(classes[j])) {
                var parts = classes[j].split('-');
                y = parseInt(parts[2]) - 1;
                x = parseInt(parts[3]) - 1;
                break;
            }
        }

        if (x >= 0 && y >= 0) {
            value = div.getElementsByClassName("tile-inner")[0].innerHTML;
        }

        if (x >= 0 && y >= 0 && value > 0) {
            tileBoard[x][y] = parseInt(value);
        }
    }

    return JSON.stringify(tileBoard);
}