﻿using Bot2048.Bot;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bot2048.Helpers
{
    public class BrowserWrapper
    {
        public const string SCRIPTS_FILE_KEY = "JavascriptFileLocation";
        public const string GAME_URL_KEY = "GameUrl";

        private WebBrowser webBrowserControl;
        private bool isScriptInitialized;

        public event EventHandler<HtmlElementEventArgs> OnDocumentLoaded;

        public BrowserWrapper(WebBrowser webBrowserControl)
        {
            this.webBrowserControl = webBrowserControl;
            this.isScriptInitialized = false;
            LoadPage();
        }

        private void LoadPage()
        {
            this.webBrowserControl.Navigate(ConfigurationManager.AppSettings[GAME_URL_KEY]);
            this.webBrowserControl.Navigated += webBrowserControl_Navigated;
        }

        public void webBrowserControl_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            if (!this.isScriptInitialized && this.webBrowserControl.Document != null && this.webBrowserControl.Document.Window != null)
            {
                this.isScriptInitialized = true;
                this.webBrowserControl.Document.Window.Load += PageLoaded;
            }
        }

        private void PageLoaded(object sender, HtmlElementEventArgs e)
        {
            InjectScripts();
            ResetGame();

            if (this.OnDocumentLoaded != null)
            {
                this.OnDocumentLoaded(sender, e);
            }
        }

        private void InjectScripts()
        {
            var scriptFilePath = ConfigurationManager.AppSettings[SCRIPTS_FILE_KEY];
            var fileContent = File.ReadAllText(scriptFilePath);

            var scriptElement = this.webBrowserControl.Document.CreateElement("script");
            scriptElement.SetAttribute("type", "text/javascript");
            scriptElement.InnerText = fileContent;

            var head = GetHeadElement();
            head.AppendChild(scriptElement);
        }

        private void ResetGame()
        {
            const string resetGameFunctionName = "resetGame";
            this.webBrowserControl.Document.InvokeScript(resetGameFunctionName);
        }

        private HtmlElement GetHeadElement()
        {
            var headCollection = this.webBrowserControl.Document.GetElementsByTagName("head");
            if (headCollection.Count > 0)
            {
                return headCollection[0];
            }

            return null;
        }

        public void SimulateKey(Direction key)
        {
            if (key != Direction.Up && key != Direction.Down && key != Direction.Left && key != Direction.Right)
            {
                throw new ArgumentException("Simulated key can be only one of the following: Up, Down, Left, Right");
            }

            var jsFunctionName = "simulate" + key.ToString();
            RunOnCreationThread(this.webBrowserControl, (Action)(() =>
            {
                this.webBrowserControl.Document.InvokeScript(jsFunctionName);
            }));
        }

        public void StartNewGame()
        {
            var jsFunctionName = "startNewGame";
            RunOnCreationThread(this.webBrowserControl, (Action)(() =>
            {
                this.webBrowserControl.Document.InvokeScript(jsFunctionName);
            }));
        }

        private void RunOnCreationThread(WebBrowser browser, Action action)
        {
            if (browser.InvokeRequired)
            {
                var result = browser.Invoke(action);
            }
            else
            {
                action();
            }
        }

        public int[,] GetBoardConfiguration()
        {
            const string GET_BOARD_METHOD_NAME = "getBoard";

            object result = null;
            RunOnCreationThread(this.webBrowserControl, (Action)(() =>
            {
                result = this.webBrowserControl.Document.InvokeScript(GET_BOARD_METHOD_NAME);
            }));

            return JsonConvert.DeserializeObject<int[,]>(result.ToString());
        }
    }
}
