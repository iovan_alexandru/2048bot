﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bot2048.Helpers
{
    public class ConfigReader
    {
        private const string GAMEPLAY_TIME_DEFAULT_VALUE_KEY = "GameplayTimeDefaultValue";
        private const string MOVE_DELAY_KEY = "MoveDelay";

        /// <summary>
        /// Returns the default gameplay value time.
        /// </summary>
        public static int DefaultGameplayTime
        {
            get
            {
                var defaultTimeConfigValue = ConfigurationManager.AppSettings[GAMEPLAY_TIME_DEFAULT_VALUE_KEY];
                int defaultTime = 0;
                if (string.IsNullOrEmpty(defaultTimeConfigValue) || !int.TryParse(defaultTimeConfigValue, out defaultTime) || defaultTime <= 0)
                {
                    throw new ArgumentException(string.Format("Invalid '{0}' key. The value should be a positive number.", GAMEPLAY_TIME_DEFAULT_VALUE_KEY));
                }

                return defaultTime;
            }
        }

        /// <summary>
        /// Returns the delay time from the configuration files.
        /// </summary>
        public static int MoveDelayMilliseconds
        {
            get
            {
                var defaultDelayTimeConfigValue = ConfigurationManager.AppSettings[MOVE_DELAY_KEY];
                int defaultDelayTime = 0;
                if (string.IsNullOrEmpty(defaultDelayTimeConfigValue) || !int.TryParse(defaultDelayTimeConfigValue, out defaultDelayTime) || defaultDelayTime < 0)
                {
                    throw new ArgumentException(string.Format("Invalid '{0}' key. The value should be a positive number.", MOVE_DELAY_KEY));
                }

                return defaultDelayTime;
            }
        }
    }
}
