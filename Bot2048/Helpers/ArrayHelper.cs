﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bot2048.Helpers
{
    public static class ArrayHelper
    {
        public static int[] GetColumn(int[,] board, int columnIndex)
        {
            var size = board.GetLength(0);
            var column = new int[size];

            for (var i = 0; i < size; i++)
            {
                column[i] = board[i, columnIndex];
            }

            return column;
        }

        public static int[] GetRow(int[,] board, int rowIndex)
        {
            var size = board.GetLength(0);
            var row = new int[size];

            for (var i = 0; i < size; i++)
            {
                row[i] = board[rowIndex, i];
            }

            return row;
        }

        public static int[] CollapseToFirst(IEnumerable<int> array)
        {
            int size = array.Count();
            int[] collapsedArray = new int[size];
            int nextSquare = 0;
            for (int i = 0; i < size; i++)
            {
                int found = 0;
                for (int j = nextSquare; j < size; j++)
                {
                    int value = array.ElementAt(j);
                    if (value == 0) continue;
                    if (found != 0)
                    {
                        if (value == found)
                        {
                            found = 2 * found;
                            nextSquare = j + 1;
                            break;
                        }
                        else if (j != 0)
                        {
                            break;
                        }
                    }
                    else
                    {
                        found = value;
                        nextSquare = j + 1;
                    }
                }
                collapsedArray[i] = found;
            }

            return collapsedArray;
        }

        public static int[] CollapseToLast(int[] array)
        {
            return CollapseToFirst(array.Reverse()).Reverse().ToArray();
        }

        public static void ReplaceColumn(int[,] board, int[] column, int columnIndex)
        {
            var size = column.Length;
            for (var i = 0; i < size; i++)
            {
                board[i, columnIndex] = column[i];
            }
        }

        public static void ReplaceRow(int[,] board, int[] row, int rowIndex)
        {
            var size = row.Length;
            for (var i = 0; i < size; i++)
            {
                board[rowIndex, i] = row[i];
            }
        }

        public static int[,] Copy(int[,] board)
        {
            var sizeX = board.GetLength(0);
            var sizeY = board.GetLength(1);

            var newBoard = new int[sizeX, sizeY];
            for (var i = 0; i < sizeX; i++)
            {
                for (var j = 0; j < sizeY; j++)
                {
                    newBoard[i, j] = board[i, j];
                }
            }

            return newBoard;
        }
    }
}
